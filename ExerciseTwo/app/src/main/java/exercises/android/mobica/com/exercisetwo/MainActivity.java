package exercises.android.mobica.com.exercisetwo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.Calendar;


public class MainActivity extends ActionBarActivity {

    private static final String PREFERENCE_KEY_GENDER_RADIO_BUTTON_ID= "preference_key_gender_radio_button_id";
    private static final String PREFERENCE_KEY_FIRST_NAME_STRING= "preference_key_first_name";
    private static final String PREFERENCE_KEY_LAST_NAME_STRING= "preference_key_last_name";
    private static final String PREFERENCE_KEY_MARRIED_BOOLEAN= "preference_key_married";
    private static final String PREFERENCE_KEY_DATE_DAY_INTEGER= "preference_key_date_day";
    private static final String PREFERENCE_KEY_DATE_MONTH_INTEGER= "preference_key_date_month";
    private static final String PREFERENCE_KEY_DATE_YEAR_INTEGER= "preference_key_date_year";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            onSaveButtonPressed(item.getActionView());
            return true;
        }
        if (id == R.id.action_load) {
            onLoadButtonPressed(item.getActionView());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSaveButtonPressed(View view) {

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String firstName= ((EditText)findViewById(R.id.first_name_edit_text)).getText().toString();
        String lastName= ((EditText) findViewById(R.id.last_name_edit_text)).getText().toString();

        if("".equals(firstName) || "".equals(lastName)) {

            Toast.makeText(this, getString(R.string.empty_first_or_last_name), Toast.LENGTH_LONG).show();
            return;
        }

        sharedPreferences.edit()
                .putString(PREFERENCE_KEY_FIRST_NAME_STRING, firstName)
                .putString(PREFERENCE_KEY_LAST_NAME_STRING, lastName)
                .putBoolean(PREFERENCE_KEY_MARRIED_BOOLEAN, ((CheckBox) findViewById(R.id.married_checkbox)).isChecked())
                .putInt(PREFERENCE_KEY_GENDER_RADIO_BUTTON_ID, ((RadioGroup) findViewById(R.id.gender_radio_group)).getCheckedRadioButtonId())
                .putInt(PREFERENCE_KEY_DATE_DAY_INTEGER, ((DatePicker)findViewById(R.id.date_picker)).getDayOfMonth())
                .putInt(PREFERENCE_KEY_DATE_MONTH_INTEGER, ((DatePicker)findViewById(R.id.date_picker)).getMonth())
                .putInt(PREFERENCE_KEY_DATE_YEAR_INTEGER, ((DatePicker)findViewById(R.id.date_picker)).getYear())
                .apply();

        Toast.makeText(this, getString(R.string.saved_toast), Toast.LENGTH_SHORT).show();

    }

    public void onLoadButtonPressed(View view) {

        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String firstName= sharedPreferences.getString(PREFERENCE_KEY_FIRST_NAME_STRING, "");
        String lastName= sharedPreferences.getString(PREFERENCE_KEY_LAST_NAME_STRING, "");
        Boolean isMarried= sharedPreferences.getBoolean(PREFERENCE_KEY_MARRIED_BOOLEAN, false);
        int radioButtonId= sharedPreferences.getInt(PREFERENCE_KEY_GENDER_RADIO_BUTTON_ID, R.id.male_radio_button);
        int dayOfMonth= sharedPreferences.getInt(PREFERENCE_KEY_DATE_DAY_INTEGER, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        int month= sharedPreferences.getInt(PREFERENCE_KEY_DATE_MONTH_INTEGER, Calendar.getInstance().get(Calendar.MONTH));
        int year= sharedPreferences.getInt(PREFERENCE_KEY_DATE_YEAR_INTEGER, Calendar.getInstance().get(Calendar.YEAR));



        ((EditText) findViewById(R.id.first_name_edit_text)).setText(firstName);
        ((EditText) findViewById(R.id.last_name_edit_text)).setText(lastName);
        ((CheckBox) findViewById(R.id.married_checkbox)).setChecked(isMarried);
        ((RadioGroup) findViewById(R.id.gender_radio_group)).check(radioButtonId);
        ((DatePicker) findViewById(R.id.date_picker)).updateDate(year, month, dayOfMonth);

        Toast.makeText(this, getString(R.string.loaded_toast), Toast.LENGTH_SHORT).show();

    }

}
