package exercises.android.mobica.com.exerciseone;

import android.app.*;
import android.database.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import java.net.*;

import butterknife.*;
import exercises.android.mobica.com.exerciseone.database.*;
import timber.log.*;


public class MainFragment extends Fragment implements UrlTestInitiator {

    @InjectView(R.id.url_edit_text) EditText mUrlEditText;
    @InjectView(R.id.url_listview) ListView mUrlListView;

    private OnFragmentInteractionListener mListenerActivity;
    private ExerciseOneDatabaseHelper databaseHelper;
    private TestPerformer testPerformer;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListenerActivity = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        setRetainInstance(true);
        setTestPerformer();
        populateUrlListView();

        return view;
    }

    private void setTestPerformer() {
        try {
            TestPerformer.bindToActivity(getActivity());
            testPerformer= TestPerformer.getInstance();
        } catch (java.lang.InstantiationException e) {
            Timber.e(e.getMessage());
        }
    }

    private void populateUrlListView() {
        databaseHelper = ExerciseOneDatabaseHelper.getInstance();
        mUrlListView.setAdapter(new MainAdapter(getActivity().getApplicationContext(), null, false));

        (new UpdateListviewTask()).execute();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        } catch (NullPointerException e) {
            Timber.i("The holding activity has no action bar");
        }
    }

    @OnItemLongClick(R.id.url_listview)
    public boolean onUrlListViewItemLongClick(int position) {
        Cursor itemCursor = (Cursor) mUrlListView.getItemAtPosition(position);
        String url = itemCursor.getString(itemCursor.getColumnIndexOrThrow(DatabaseSchemaV2.URL));
        showUrlItemDetailFragment(url);
        return false;
    }

    private void showUrlItemDetailFragment(String urlItemText) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.main_activity_layout, UrlItemDetailsFragment.getNewInstance(urlItemText))
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @OnEditorAction(R.id.url_edit_text)
    boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE)
            performUrlTestAction();

        return false;
    }

    @OnClick(R.id.test_button)
    public void performUrlTestAction() {
        try {
            URL urlToCheck = new URL(mUrlEditText.getText().toString());
            testPerformer.performTest(urlToCheck, this);

        } catch (MalformedURLException e) {
            mUrlEditText.setError(getString(R.string.bad_url_message));
            mUrlEditText.selectAll();
        }
    }

    @Override
    public void onUrlTestPerformed() {
        (new UpdateListviewTask()).execute();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUrlListView != null) {
            ((CursorAdapter) mUrlListView.getAdapter()).getCursor().close();
        }
        ButterKnife.reset(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListenerActivity = null;
    }

    interface OnFragmentInteractionListener {
    }

    class UpdateListviewTask extends BackgroundTask<Void, Cursor> {

        protected UpdateListviewTask() {
            super(null);
        }

        @Override
        protected Cursor doTask(Void arguments) throws Exception {
            Cursor result = databaseHelper.getReadableDatabase()
                    .rawQuery(String.format(
                            "SELECT ROWID AS _id, %1$s, %2$s FROM %3$s WHERE ROWID IN " +
                                    "(SELECT MAX(ROWID) FROM %3$s GROUP BY %1$s) ORDER BY %4$s DESC;",
                            DatabaseSchemaV2.URL,
                            DatabaseSchemaV2.RESULT,
                            DatabaseSchemaV2.TABLE,
                            DatabaseSchemaV2.TIMESTAMP), null);

            result.getCount(); //touching cursor to execute task in this thread.

            return result;
        }

        @Override
        protected void onTaskDone(Cursor resultCursor) {
            ((CursorAdapter) mUrlListView.getAdapter()).changeCursor(resultCursor);
            //It closes previous cursor- nice :)
        }

        @Override
        protected void onTaskException(Exception exception) {
            Timber.e("Update listview cursor gone wrong :(");
        }
    }

}
