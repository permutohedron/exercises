package exercises.android.mobica.com.exerciseone;

import android.app.*;
import android.database.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;

import java.net.*;

import butterknife.*;
import exercises.android.mobica.com.exerciseone.database.*;
import timber.log.*;


public class UrlItemDetailsFragment extends Fragment implements UrlTestInitiator {

    public static final String ARG_URL = "url";

    @InjectView(R.id.url_item_details_fragment_listview) ListView listView;

    private OnFragmentInteractionListener mActivityListener;
    private String url;
    private ExerciseOneDatabaseHelper databaseHelper;
    private TestPerformer testPerformer;

    public UrlItemDetailsFragment() {
        this.databaseHelper = ExerciseOneDatabaseHelper.getInstance();
    }

    public static UrlItemDetailsFragment getNewInstance(String url) {
        UrlItemDetailsFragment fragment = new UrlItemDetailsFragment();

        Bundle arguments = new Bundle();
        arguments.putString(ARG_URL, url);

        fragment.setArguments(arguments);
        fragment.setHasOptionsMenu(true);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mActivityListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.url = getArguments().getString(ARG_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentLayout = inflater.inflate(R.layout.fragment_url_item_details, container, false);
        ButterKnife.inject(this, fragmentLayout);
        setRetainInstance(true);
        listView.setAdapter(new UrlItemDetailsAdapter(getActivity().getApplicationContext(), null, false));
        setTestPerformer();
        (new UpdateUrlDetailsListview()).execute();

        return fragmentLayout;
    }

    private void setTestPerformer() {
        try {
            TestPerformer.bindToActivity(getActivity());
            testPerformer= TestPerformer.getInstance();
        } catch (java.lang.InstantiationException e) {
            Timber.e(e.getMessage());
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(url);
        } catch (NullPointerException e) {
            Timber.i("The holding activity has no action bar");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.url_item_details_fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_action_refresh) {
            performUrlTestAction();
            return false;
        } else return super.onOptionsItemSelected(item);
    }

    private void performUrlTestAction() {
        try {
            testPerformer.performTest(new URL(url), this);
        } catch (MalformedURLException e) {
            Timber.e("Malformed Url. Now way to get this here");
        }
    }

    @Override
    public void onUrlTestPerformed() {
        (new UpdateUrlDetailsListview()).execute();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (listView != null) {
            ((CursorAdapter) listView.getAdapter()).getCursor().close();
        }
        ButterKnife.reset(this);
    }

    public interface OnFragmentInteractionListener {
    }

    class UpdateUrlDetailsListview extends BackgroundTask<Void, Cursor> {

        UpdateUrlDetailsListview() {
            super(null);
        }

        @Override
        protected Cursor doTask(Void args) throws Exception{
            String url = UrlItemDetailsFragment.this.url;
            Cursor result = databaseHelper.getReadableDatabase().query(
                    DatabaseSchemaV2.TABLE,
                    new String[]{"ROWID AS _id", DatabaseSchemaV2.RESULT, DatabaseSchemaV2.TIMESTAMP},
                    DatabaseSchemaV2.URL + " = ?",
                    new String[]{url},
                    null,
                    null,
                    DatabaseSchemaV2.TIMESTAMP + " DESC"
            );
            return result;
        }

        @Override
        protected void onTaskDone(Cursor results) {
            ((CursorAdapter) listView.getAdapter()).changeCursor(results);
        }

        @Override
        protected void onTaskException(Exception exception) {
            Timber.e(exception.getMessage());
        }

    }

}
