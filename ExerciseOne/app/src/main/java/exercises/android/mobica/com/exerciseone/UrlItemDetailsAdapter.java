package exercises.android.mobica.com.exerciseone;

import android.content.*;
import android.database.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import butterknife.*;
import exercises.android.mobica.com.exerciseone.database.*;

public class UrlItemDetailsAdapter extends CursorAdapter {

    public UrlItemDetailsAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.timestamp_list_item_view, parent, false);
        view.setTag(new ViewHolder(view));

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor row) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.bindData(new Data(row));
    }


    private static class ViewHolder {
        ImageView statusIcon;
        TextView timestampText;

        ViewHolder(View view) {
            statusIcon = ButterKnife.findById(view, R.id.timestamp_view_status_icon);
            timestampText = ButterKnife.findById(view, R.id.timestamp_view_text);
        }

        public void bindData(Data data) {
            statusIcon.setImageResource(data.statusIconResourceId);
            timestampText.setText(data.timestampText);
        }

    }

    private static class Data {
        private String timestampText;
        private int statusIconResourceId;

        Data(Cursor cursor) {
            timestampText= getTimestampTextFromRow(cursor);
            statusIconResourceId= (isStatusInRowSuccess(cursor) ? R.drawable.circle_green : R.drawable.circle_red);
        }

        private String getTimestampTextFromRow(Cursor cursor) {
            int positionOfTimestampLongValue = cursor.getColumnIndexOrThrow(DatabaseSchemaV2.TIMESTAMP);

            Date date = new Date(cursor.getLong(positionOfTimestampLongValue));
            return date.toString();
        }

        private boolean isStatusInRowSuccess(Cursor cursor) {
            int positionOfResult = cursor.getColumnIndexOrThrow(DatabaseSchemaV2.RESULT);
            return cursor.getInt(positionOfResult) != 0;
        }
    }
}
