package exercises.android.mobica.com.exerciseone;

abstract class ResultOrException<ResultType> {

    public static <T> ResultOrException<T> getResultKind(T result) {
        return new Result<>(result);
    }

    public static <C> ResultOrException<C> getExceptionKind(java.lang.Exception exception) {
        return new Exception<>(exception);
    }

    abstract boolean isException();

    abstract java.lang.Exception getException();

    abstract public ResultType getResult();

    private static class Exception<C> extends ResultOrException<C> {

        private final java.lang.Exception exception;

        private Exception(java.lang.Exception exception) {
            this.exception = exception;
        }

        @Override
        boolean isException() {
            return true;
        }

        @Override
        java.lang.Exception getException() {
            return exception;
        }

        @Override
        public C getResult() {
            throw new IllegalStateException();
        }
    }

    private static class Result<T> extends ResultOrException<T> {

        private final T result;

        private Result(T result) {
            this.result= result;
        }

        @Override
        boolean isException() {
            return false;
        }

        @Override
        java.lang.Exception getException() {
            throw new IllegalStateException();
        }

        @Override
        public T getResult() {
            return result;
        }
    }
}
