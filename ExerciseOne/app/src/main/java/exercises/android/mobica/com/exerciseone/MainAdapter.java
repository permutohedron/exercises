package exercises.android.mobica.com.exerciseone;

import android.content.*;
import android.database.*;
import android.view.*;
import android.widget.*;

import butterknife.*;
import exercises.android.mobica.com.exerciseone.database.*;

public class MainAdapter extends CursorAdapter {

    public MainAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.url_list_item_view, parent, false);
        view.setTag(new ViewHolder(view));

        return view;
        //CursorAdapter binds new view by itself
    }

    @Override
    public void bindView(View view, Context context, Cursor row) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.bindData(new Data(row));
    }


    private static class ViewHolder {
        ImageView statusIcon;
        TextView urlText;

        ViewHolder(View view) {
            statusIcon = ButterKnife.findById(view, R.id.url_view_status_icon);
            urlText = ButterKnife.findById(view, R.id.url_view_text);
        }

        public void bindData(Data data) {
            statusIcon.setImageResource(data.statusIconResourceId);
            urlText.setText(data.urlText);
        }
    }

    public static class Data {
        private String urlText;
        private int statusIconResourceId;

        Data(Cursor cursor) {
            int urlTextPosition = cursor.getColumnIndexOrThrow(DatabaseSchemaV2.URL);
            urlText = cursor.getString(urlTextPosition);
            statusIconResourceId = isStatusInRowSuccess(cursor) ?
                    R.drawable.circle_green : R.drawable.circle_red;
        }

        private boolean isStatusInRowSuccess(Cursor cursor) {
            int positionOfResult = cursor.getColumnIndexOrThrow(DatabaseSchemaV2.RESULT);
            return cursor.getInt(positionOfResult) != 0;
        }
    }
}
