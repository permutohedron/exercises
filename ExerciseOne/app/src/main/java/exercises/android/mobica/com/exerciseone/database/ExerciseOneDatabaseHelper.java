package exercises.android.mobica.com.exerciseone.database;

import android.content.*;
import android.database.sqlite.*;

public class ExerciseOneDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "exercise_one.db";

    private static ExerciseOneDatabaseHelper exerciseOneDatabaseHelper;
    private static DatabaseSchemaManager databaseSchemaManager;

    static {
        databaseSchemaManager = DatabaseSchemaManager.getManagerInstance();
        databaseSchemaManager
                .addNewSchema(new DatabaseSchemaV1())
                .addNewSchema(new DatabaseSchemaV2()); //current schema
    }

    private ExerciseOneDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DatabaseSchemaManager.getManagerInstance().getCurrentSchema().getVersionNumber());
    }

    public static void initialize(Context context) {
        exerciseOneDatabaseHelper = new ExerciseOneDatabaseHelper(context);
    }

    public static ExerciseOneDatabaseHelper getInstance() {
        if (exerciseOneDatabaseHelper == null) {
            throw new RuntimeException("ExerciseOneDatabaseHelper has not been initialized. Has to be initialized before invoking bindToActivity()");
        }
        return exerciseOneDatabaseHelper;
    }

    public static void finish() {
        if (exerciseOneDatabaseHelper != null) {
            exerciseOneDatabaseHelper.close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        databaseSchemaManager.getCurrentSchema().createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        databaseSchemaManager.getCurrentSchema().updateDatabaseToThisFromVersion(db, oldVersion);
    }
}
