package exercises.android.mobica.com.exerciseone;

import android.os.*;

public abstract class BackgroundTask<ArgumentType, ResultType>
        extends AsyncTask<Void, Void, ResultOrException<ResultType>> {

    private final ArgumentType argument;

    protected BackgroundTask(ArgumentType argument) {
        this.argument = argument;
    }

    @Override
    protected final ResultOrException<ResultType> doInBackground(Void... params) {
        try {
            return ResultOrException.getResultKind(doTask(argument));
        } catch (Exception e) {
            return ResultOrException.getExceptionKind(e);
        }
    }

    @Override
    protected final void onPostExecute(ResultOrException<ResultType> resultOrException) {
        if (resultOrException.isException()) {
            onTaskException(resultOrException.getException());
        } else {
            onTaskDone(resultOrException.getResult());
        }
    }

    protected abstract ResultType doTask(ArgumentType arguments) throws Exception;

    protected abstract void onTaskDone(ResultType results);

    protected abstract void onTaskException(Exception exception);

}