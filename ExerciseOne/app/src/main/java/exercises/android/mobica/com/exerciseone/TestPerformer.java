package exercises.android.mobica.com.exerciseone;

import android.app.*;
import android.content.*;
import android.net.*;
import android.widget.*;

import java.io.*;
import java.net.*;

import exercises.android.mobica.com.exerciseone.database.*;
import timber.log.*;

public class TestPerformer {

    private static TestPerformer instance;
    private ProgressDialog progressDialog;
    private final Activity activity;
    private String url;
    private UrlTestInitiator caller;

    private TestPerformer(Activity activity) {
        this.activity = activity;
    }

    public static void bindToActivity(Activity activity) throws InstantiationException {
        if(instance == null) {
            instance = new TestPerformer(activity);
        }
        if(instance.activity != activity) {
            throw new InstantiationException("TestPerformer is bound to different activity.");
        }
    }

    public static TestPerformer getInstance() throws InstantiationException{
        if(instance != null) {
            return instance;
        }
        throw new InstantiationException("Use bindToActivity(Activity activity) first");
    }


    public void performTest(URL urlToCheck, UrlTestInitiator caller) {
        if (isTestInProgress()) return;
        if (!isInternetConnectionAvailable()) {
            Toast.makeText(activity, activity.getString(R.string.no_internet_connection_message), Toast.LENGTH_LONG).show();
            return;
        }

        this.url = urlToCheck.toString();
        this.caller = caller;

        (new TestUrlTask(urlToCheck)).execute();
        showProgressDialog();

    }

    private boolean isTestInProgress() {
        return progressDialog != null;
    }

    private void showProgressDialog() {
        progressDialog =
                ProgressDialog.show(activity, activity.getString(R.string.loading), activity.getString(R.string.loadingMessage));
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
        progressDialog = null;
    }

    private void onUrlAvailabilityChecked(boolean isUrlAvailable) {

        hideProgressDialog();

        Toast.makeText(activity, activity.getString(isUrlAvailable ? R.string.successString : R.string.failureString), Toast.LENGTH_LONG).show();

        ContentValues valuesToInsert = new ContentValues();
        valuesToInsert.put(DatabaseSchemaV2.URL, url);
        valuesToInsert.put(DatabaseSchemaV2.RESULT, isUrlAvailable);
        valuesToInsert.put(DatabaseSchemaV2.TIMESTAMP, System.currentTimeMillis());
        (new InsertToDatabaseTask(valuesToInsert, new InsertToDatabaseTask.DatabaseInsertionListener() {
            @Override
            public void onDatabaseInsertionDone() {
                caller.onUrlTestPerformed();
            }

            @Override
            public void onDatabaseInsertionException(Exception e) {
            }
        })).execute();
    }

    private boolean isInternetConnectionAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }

    private class TestUrlTask extends BackgroundTask<URL, Boolean> {

        protected TestUrlTask(URL argument) {
            super(argument);
        }

        @Override
        protected Boolean doTask(URL url) throws Exception {
            HttpURLConnection httpURLConnection;
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            httpURLConnection.disconnect();

            return responseCode == 200;
        }

        @Override
        protected void onTaskDone(Boolean isUrlAvailable) {
            onUrlAvailabilityChecked(isUrlAvailable);
        }

        @Override
        protected void onTaskException(Exception exception) {
            hideProgressDialog();
            if (exception instanceof IOException) {
                onTaskDone(false); //Usually gets here when url with no host ex. www.cnn.no.such.domain. We regard this as failure.
            }
            Timber.e(exception.getMessage());
        }
    }
}
