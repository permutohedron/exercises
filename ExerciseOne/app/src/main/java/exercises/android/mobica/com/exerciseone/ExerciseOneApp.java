package exercises.android.mobica.com.exerciseone;

import android.app.*;

import exercises.android.mobica.com.exerciseone.database.*;
import timber.log.*;

public class ExerciseOneApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        ExerciseOneDatabaseHelper.initialize(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ExerciseOneDatabaseHelper.finish();
    }
}
