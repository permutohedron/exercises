package exercises.android.mobica.com.exerciseone.database;

import android.database.sqlite.*;

import java.util.*;

public class DatabaseSchemaManager {
    private static LinkedList<DatabaseSchema> orderedSchemas = new LinkedList<>();
    private static DatabaseSchemaManager managerInstance = new DatabaseSchemaManager();

    private DatabaseSchemaManager() {
    }

    public static DatabaseSchemaManager getManagerInstance() {
        if (managerInstance == null) {
            managerInstance = new DatabaseSchemaManager();
        }
        return managerInstance;
    }

    public DatabaseSchemaManager addNewSchema(DatabaseSchema newSchema) {
        newSchema.setVersionNumber(orderedSchemas.size() + 1);
        orderedSchemas.add(newSchema);
        return this;
    }

    public DatabaseSchema getCurrentSchema() {
        return orderedSchemas.getLast();
    }

    public static abstract class DatabaseSchema {
        private int schemaVersionNumber;

        abstract public void createTables(SQLiteDatabase database);

        abstract public void updateFromPreviousSchemaVersion(SQLiteDatabase database);

        public void updateDatabaseToThisFromVersion(SQLiteDatabase database,
                                                    int fromSchemaVersionNumber
        ) {

            Iterator<DatabaseSchema> databaseReversedSchemasIterator = orderedSchemas.descendingIterator();
            while (databaseReversedSchemasIterator.hasNext()) {
                DatabaseSchema schema = databaseReversedSchemasIterator.next();
                if (schema == this) break;//move to this schema in iterator
            }

            updateDatabase(database, databaseReversedSchemasIterator, fromSchemaVersionNumber);
        }

        private void updateDatabase(SQLiteDatabase database,
                                    Iterator<DatabaseSchema> databaseReversedSchemasIterator,
                                    int toUpdateFromSchemaVersionNumber
        ) {
            if (databaseReversedSchemasIterator.hasNext()
                    && isMoreThanOneVersionBeforeSchemaVersionToUpdateFrom(toUpdateFromSchemaVersionNumber)
                    ) {
                databaseReversedSchemasIterator.next()
                        .updateDatabase(database,
                                databaseReversedSchemasIterator,
                                toUpdateFromSchemaVersionNumber);
            }
            updateFromPreviousSchemaVersion(database);
        }

        private boolean isMoreThanOneVersionBeforeSchemaVersionToUpdateFrom(int toUpdateFromSchemaVersionNumber) {
            return getVersionNumber() > toUpdateFromSchemaVersionNumber + 1;
        }

        public int getVersionNumber() {
            return schemaVersionNumber;
        }

        private void setVersionNumber(int versionNumber) {
            this.schemaVersionNumber = versionNumber;
        }
    }

}
