package exercises.android.mobica.com.exerciseone.database;

import android.database.sqlite.*;

import java.util.*;

public class DatabaseSchemaV2 extends DatabaseSchemaV1 {

    //The rest of static Strings are inherited from DatabaseSchemaV1
    public static final String TIMESTAMP = "timestamp";

    @Override
    public void createTables(SQLiteDatabase database) {
        database.execSQL(String.format((Locale) null, "CREATE TABLE %s (%s VARCHAR, %s BOOLEAN, %s INTEGER);", TABLE, URL, RESULT, TIMESTAMP));
    }

    @Override
    public void updateFromPreviousSchemaVersion(SQLiteDatabase database) {
        database.execSQL(String.format((Locale) null, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT 0", TABLE, TIMESTAMP));
    }
}
