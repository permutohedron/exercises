package exercises.android.mobica.com.exerciseone;

public interface UrlTestInitiator {
    void onUrlTestPerformed();
}
