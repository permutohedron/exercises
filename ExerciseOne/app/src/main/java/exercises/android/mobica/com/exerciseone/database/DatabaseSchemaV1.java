package exercises.android.mobica.com.exerciseone.database;

import android.database.sqlite.*;

import java.util.*;

public class DatabaseSchemaV1 extends DatabaseSchemaManager.DatabaseSchema {

    public static final String URL = "url";
    public static final String RESULT = "result";
    public static final String TABLE = "url_results";

    @Override
    public void createTables(SQLiteDatabase database) {
        database.execSQL(String.format((Locale) null, "CREATE TABLE %s (%s VARCHAR, %s BOOLEAN);", TABLE, URL, RESULT));
    }

    @Override
    public void updateFromPreviousSchemaVersion(SQLiteDatabase database) {

    }
}
