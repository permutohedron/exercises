package exercises.android.mobica.com.exerciseone.database;

import android.content.*;
import android.database.sqlite.*;

import exercises.android.mobica.com.exerciseone.*;

public class InsertToDatabaseTask extends BackgroundTask<ContentValues, Void> {

    private static ExerciseOneDatabaseHelper databaseHelper = ExerciseOneDatabaseHelper.getInstance();
    private DatabaseInsertionListener listener = null;

    public InsertToDatabaseTask(ContentValues values) {
        super(values);
    }

    public InsertToDatabaseTask(ContentValues values, DatabaseInsertionListener listener) {
        super(values);
        this.listener = listener;
    }

    @Override
    protected Void doTask(ContentValues valuesToInsertToDatabase) throws Exception {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        long resultOfInsertion =
                database.insert(DatabaseSchemaV2.TABLE, DatabaseSchemaV2.URL, valuesToInsertToDatabase);
        if (resultOfInsertion == -1) {
            //error occurred
            throw new SQLiteException();
        }
        return null;
    }

    @Override
    protected void onTaskDone(Void results) {
        if (listener != null) {
            listener.onDatabaseInsertionDone();
        }
    }

    @Override
    protected void onTaskException(Exception exception) {
        if (listener != null){
            listener.onDatabaseInsertionException(exception);
        }
    }

    public interface DatabaseInsertionListener {
        void onDatabaseInsertionDone();
        void onDatabaseInsertionException(Exception e);
    }
}
