package exercises.android.mobica.com.exerciseone;

import android.os.*;
import android.support.v7.app.*;

import butterknife.*;

/**
 * Launcher {@link android.app.Activity} to execute url connection when Test {@link android.widget.Button} clicked.
 * App tries connect to http://www.mobica.com in the background, displays {@link android.app.ProgressDialog} when performing so and displays
 * "SUCCESS" {@link android.widget.Toast} in case of HTTP 200 response or "FAILURE" otherwise.
 */

public class MainActivity extends ActionBarActivity implements
        MainFragment.OnFragmentInteractionListener,
        UrlItemDetailsFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFragmentManager()
                .beginTransaction()
                .add(R.id.main_activity_layout, new MainFragment())
                .commit();
        ButterKnife.inject(this);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
